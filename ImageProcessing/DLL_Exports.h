#pragma once

#include "stdafx.h"

#define API    extern "C"  __declspec(dllexport)


API void  SetImageDetails(void* data, int Height, int Width );
API void* ProcessWithCanny();
API void* ProcessWithDBSCAN();
API void* ProcessWithChanVese();