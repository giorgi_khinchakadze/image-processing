#pragma once

#include "GlobalVars.h"

vector< vector<int> > SobelX{ { -1,0,1 },{ -2,0,2 },{ -1,0,1 } };
vector< vector<int> > SobelY{ { -1,-2,-1 },{ 0,0,0 },{ 1,2,1 } };

Gradient getSobelValue(int x, int y)
{

	int Gx = 0;
	int Gy = 0;

	Gx = getConvolutionValueGS(x, y, SobelX);
	Gy = getConvolutionValueGS(x, y, SobelY);

	Gradient tmp(Gx, Gy);

	return  tmp;
}


// Needs Post Processing
void PopulateSobelGS()
{
	int cp = 0;
	for (int y = 0; y < image->getHeight(); y++) {
		for (int x = 0; x < image->getWidth(); x++) {
			sobelG[cp] = getSobelValue(x, y);
			sobel[cp++] = sobelG[cp].value;
		}
	}
}

