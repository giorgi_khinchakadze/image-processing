#pragma once

#include "GlobalVars.h"

vector< vector<int> > RobertsX{ { 0,0,0 },{ -1,1,0 },{ 0,0,0 } };
vector< vector<int> > RobertsY{ { 0,0,0 },{ 0,1,0 },{ 0,-1,0 } };


Gradient getRobertsValue(int x, int y)
{

	int Gx = 0;
	int Gy = 0;

	Gx = getConvolutionValueGS(x, y, RobertsX);
	Gy = getConvolutionValueGS(x, y, RobertsY);

	Gradient tmp(Gx, Gy);

	return  tmp;
}

// Needs Post Processing
void PopulateRobertsGS()
{
	int cp = 0;
	for (int y = 0; y < image->getHeight(); y++) {
		for (int x = 0; x < image->getWidth(); x++) {
			robertsG[cp] = getRobertsValue(x, y);
			roberts[cp++] = robertsG[cp].value;
		}
	}
}




