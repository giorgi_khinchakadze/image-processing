#pragma once
#include "GlobalVars.h"

vector< vector<int> > PrewittX{ { -1,0,1 },{ -1,0,1 },{ -1,0,1 } };
vector< vector<int> > PrewittY{ { -1,-1,-1 },{ 0,0,0 },{ 1,1,1 } };



Gradient getPrewittValue(int x, int y)
{

	int Gx = 0;
	int Gy = 0;

	Gx = getConvolutionValueGS(x, y, PrewittX);
	Gy = getConvolutionValueGS(x, y, PrewittY);

	Gradient tmp(Gx, Gy);

	return  tmp;
}


// Needs Post Processing
void PopulatePrewittGS()
{
	int cp = 0;
	for (int y = 0; y < image->getHeight(); y++) {
		for (int x = 0; x < image->getWidth(); x++) {
			prewittG[cp] = getPrewittValue(x, y);
			prewitt[cp++] = prewittG[cp].value;
		}
	}
}


