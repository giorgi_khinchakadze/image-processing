#pragma once

double getConvolutionValue(BYTE*, int, int,int,int, double, vector< vector<double> >);
void PopulateSobel();
void PopulateDBSCAN();
void PopulateChanVese();

void PopulateSobelWithGPU();


