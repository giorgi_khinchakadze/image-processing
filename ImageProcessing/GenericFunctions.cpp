#include "stdafx.h"
#include "Functions.h"



double getConvolutionValue(BYTE* value, int x,int y,int Height, int Width, double multiplier, vector< vector<double> > kernel)
{
		int r = kernel.size() / 2;
		int l = 0 - r;
		double tmp = 0;

		for (int i = l; i <= r; i++)
		{
			for (int j = l; j <= r; j++)
			{
				int tmpx = x + j;
				int tmpy = y + i;

				if (tmpx < 0)tmpx = 0;
				if (tmpy < 0)tmpy = 0;
				if (tmpx >= Width)tmpx = Width - 1;
				if (tmpy >= Height)tmpy = Height - 1;

				tmp += value[tmpx + tmpy * Width] * kernel[i + r][j + r];
			}
		}
		tmp *= multiplier;


		return tmp;
}


