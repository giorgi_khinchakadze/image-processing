#pragma once
#include "stdafx.h"

struct Point {
	int i = 0;
	int j = 0;
public:
	Point() {
		i = 0;
		j = 0;
	}
	Point(int a, int b):i(a),j(b){}


};





struct Direction {
	int i = 0;
	int j = 0;
public:
	Direction() {
		i = 0;
		j = 0;
	}
	Direction(double Gx, double Gy)
	{
			double a = atan2(Gy, Gx) * 180 / 3.14159265 ;
			if (a < 0) a = 360 + a;

			if ((a > 360 - 45 / 2 || a < 45 / 2) || (a > 180 - 45 / 2 && a < 180 + 45 / 2)) { j = 1; i = 0; }
			else if ((a > 90 - 45 / 2 && a < 90 + 45 / 2) || (a > 270 - 45 / 2 && a < 270 + 45 / 2)) { i = 1; j = 0; }
			else if ((a > 45 / 2 && a < 90 - 45 / 2) || (a > 180 + 45 / 2 && a < 270 - 45 / 2)) { i = -1; j = 1; }
			else { i = 1; j = 1; }

	}
};


extern ofstream fout;



struct Lab{
	double L; // 0 - 100
	double a; // -128 to 127
	double b; // -128 to 127
	Lab() {
		L = 0;
		a = 0;
		b = 0;
	}

	Lab(double R, double G, double B):L(0),a(0),b(0) {

		R /= 255.000000;
		G /= 255.000000;
		B /= 255.000000;


		double X = 0.4124564 * R + 0.3575761 * G + 0.1804375 * B;
		double Y = 0.2126729 * R + 0.7151522 * G + 0.0721750 * B;
		double Z = 0.0193339 * R + 0.1191920 * G + 0.9503041 * B;

		double	Xn = 0.950455;
		double	Yn = 1.0;
		double	Zn = 1.088753;
		
		auto f = [](double x) { if (x > 0.008856) return pow(x, 0.33333333333);
										 		  return 7.787*x + 16.00 / 116.00;
		};

		L = (116.00 * f(Y / Yn) - 16);
		a = (500.00 * (f(X / Xn) - f(Y / Yn)));
		b = (200.00 * (f(Y / Yn) - f(Z / Zn)));
	}


	bool isSimilar(Lab& r) {


		double dE;
		double dL;
		double dC;
		double dH;
		double SL;
		double SC;
		double SH;
		double RT;
		double Lnot;
		double Cnot;
		double an;
		double ran;
		double Cnotp;
		double Cp1;
		double Cp2;
		double h1;
		double h2;

		double PI = 3.14159265;

		dL = r.L - L;

		Cnot = sqrt(a * a + b * b) + sqrt(r.a * r.a + r.b * r.b);
		Cnot /= 2.0;

		Lnot = L + r.L;
		Lnot /= 2.0;

		an = a + 0.5 * a * (1 - sqrt(pow(Cnot, 7) / (pow(Cnot, 7) + pow(25, 7))));
		ran = r.a + 0.5 * r.a * (1 - sqrt(pow(Cnot, 7) / (pow(Cnot, 7) + pow(25, 7))));

		Cp1 = sqrt(an * an + b * b);
		Cp2 = sqrt(ran * ran + r.b * r.b);

		dC = Cp2 - Cp1;
		Cnotp = (Cp1 + Cp2) / 2.00;

		h1 = atan2(b, an) * 180 / 3.14159265;
		if (h1 < 0) h1 += 360;
		h2 = atan2(r.b, ran) * 180 / 3.14159265;
		if (h2 < 0) h2 += 360;

		double dh;

		if (Cp1 == 0 || Cp2 == 0) dh = 0;
		else
		{
			if (abs(h1 - h2) <= 180)
			{
				dh = h2 - h1;
			}
			else
			{
				if (h2 <= h1)
				{
					dh = h2 - h1 + 360;
				}
				else
				{
					dh = h2 - h1 - 360;
				}
			}
		}
		dh /= 2.00;
		dH = 2.00 * sqrt(Cp1 * Cp2) * sin(dh * 3.14159265 / 180.00);

		double Hnot;

		if (abs(h1 - h2) > 180)
		{
			Hnot = h1 + h2 + 360.00;
		}
		else
		{
			Hnot = h1 + h2;
		}

		if (Cp1 != 0 && Cp2 != 0) Hnot /= 2.00;


		double T;

		T = 1.00 - 0.17 * cos((Hnot - 30) * 3.14159265 / 180.00) + 0.24 * cos(Hnot * 2 * 3.14159265 / 180.00) + 0.32 * cos((3 * Hnot + 6)* 3.14159265 / 180.00) - 0.20 * cos((4 * Hnot - 63)* 3.14159265 / 180.00);

		SL = 1.00 + (0.015 * (Lnot - 50) * (Lnot - 50)) / sqrt(20 + (Lnot - 50) * (Lnot - 50));
		SC = 1.00 + 0.045 * Cnotp;
		SH = 1.00 + 0.015 * Cnotp * T;

		RT = (0.00000000 - 2.00000000) * sqrt(pow(Cnot, 7) / (pow(Cnot, 7) + pow(25, 7))) * sin((60 * exp(0.00000000 - ((Hnot - 275) / 25)*((Hnot - 275) / 25))) * 3.14159265 / 180.00);

		double a1 = dL / SL;
		double a2 = dC / SC;
		double a3 = dH / SH;
		double a4 = a3 * a2  * RT;

		dE = sqrt(a1 * a1 + a2 * a2 + a3 * a3 + a4);

		if (dE <= 2.3) return true;
		return false;
	}

};





