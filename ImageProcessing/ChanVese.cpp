﻿#include "stdafx.h"
#include "Functions.h"
#include "Variable.h"
#include "HelperClasses.h"




void PopulateChanVese() {


	unique_ptr<double[]> FiOld(new double[Height * Width]());
	unique_ptr<double[]> FiNew(new double[Height * Width]());


	auto dirac = [](double a) { return 1.00 / (3.14159265 * (1.00 + a * a)); };
	auto H = [](double a) { return 0.5 * (1.000 + 2.00 * atan(a) / 3.14159265); };





	float dt = 0.5f;
	float v = 0.10f;
	float niw = 0.50f;
	float l1 = 1.00f;
	float l2 = 1.00f;


	try {
		ifstream fin("params.in");
		fin >> dt >> v >> niw >> l1 >> l2;
	}
	catch (exception e) {

	}



	//	საწყისი კონტური 
	for (int i = 0; i < Height; i++) {
		for (int j = 0; j < Width; j++) {
			FiOld[j + i * Width] = sin(3.14159265 * j / 5.000) * sin(3.14159265 * i / 5.000);		// შახმატის დაფა
		}
	}

	double tolerance = ((double)( Height * Width )) * 0.001;

	double dif = 0;

	double omega = 1.97;

	int it = 0;

	do
	{
		dif = 0.0;

		double c1 = 0.0;
		double c2 = 0.0;
		double c1n = 0.0;
		double c2n = 0.0;

		for (int i = 0; i < Height * Width; i++) {
			double I = (double)imageGS[i];
			double F = FiOld[i];

			c1 += I * H(F);
			c2 += I * (1.0000 - H(F));
			c1n += H(F);
			c2n += 1.0000 - H(F);
		}

		c1 /= c1n;
		c2 /= c2n;

		for (int i = 0; i < Height; i++) {
			for (int j = 0; j < Width; j++)
			{
				double F = FiOld[j + i * Width];
				double I = (double)imageGS[j + i * Width];
				double Fjp = FiOld[min(j + 1, Width - 1) + i * Width];
				double Fjm = j > 0 ? FiNew[j - 1 + i * Width] : FiOld[i * Width];
				double Fip = FiOld[j + min(i + 1, Height - 1) * Width];
				double Fim = i > 0 ? FiNew[j + (i - 1) * Width] : FiOld[j];
				

				double gradient = sqrt(0.00000001 + pow((Fip - Fim) / 2.0, 2) + pow((Fjp - Fjm) / 2.0, 2));

				double divp = Fip + Fim + Fjp + Fjm;

				double FN = F + dt * dirac(F) *  (niw * divp / gradient - v - l1 * pow(I - c1, 2) + l2 * pow(I - c2, 2));

				FN /= (1.0 + 4.0 * dt * dirac(F) * niw / gradient);

//				FN = (1 - omega)*F + omega*FN;

				FiNew[j + i * Width ] = FN;
				dif += pow(FN - F, 2);

			}
		}


		FiOld.swap(FiNew);
	
		dif = sqrt(dif);
		fout << dif << " " << tolerance << endl;
		
		it++;
	} while ( dif  > tolerance || it <= 5);



	for (int i = 0; i < Height * Width; i++) {
		if (FiOld[i] < 0.001 ) {
			ChanVese[i] = 255;
		}
	}

}






