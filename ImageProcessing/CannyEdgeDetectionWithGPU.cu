#include "stdafx.h"
#include "Variable.h"
#include "Functions.h"
#include "HelperClasses.h"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <thrust\device_vector.h>

// *************  Sobel ************* \\

float SobelMultiplier = 1;
vector< float > SobelKernelX{ -1,0,1 , -2,0,2  , -1,0,1 };
vector< float > SobelKernelY{ -1,-2,-1 , 0,0,0  , 1,2,1 };
int SobelKernelSize = 3;

// *************  Gaussian Filter ************* \\

float GaussianMultiplier = (1.00 / 159.00);
vector< float > GaussianKernel{ 2,4,5,4,2 , 4,9,12,9,4 , 5,12,15,12,5 , 4,9,12,9,4 ,2,4,5,4,2 };
int GaussianKernelSize = 5;


__device__ int getConvloutionValueWithGPU() {

}

__global__ void  getConvolutionWithGPU(BYTE* des, BYTE* src,float multiplier,thrust::device_vector<float> kernel,int ksize) {



}


void PopulateSobelWithGPU() {

	thrust::device_vector<float> d_GaussianKernel = GaussianKernel;

	BYTE* d_imageGS;
	BYTE* d_Canny;
	cudaMalloc((void**)&d_imageGS, (Height * Width) * sizeof(BYTE));
	cudaMalloc((void**)&d_Canny, (Height * Width) * sizeof(BYTE));

	cudaMemcpy(d_imageGS, imageGS.get(), (Height * Width) * sizeof(BYTE), cudaMemcpyHostToDevice);


#pragma region Gaussian Filter

	dim3 blocksize( Width/2, Height/2);
	getConvolutionWithGPU<<< 4, blocksize >>>(d_Canny,d_imageGS,GaussianMultiplier, d_GaussianKernel,GaussianKernelSize);



/*
	for (int i = 0; i < Height; i++) {
		for (int j = 0; j < Width; j++) {
			imageGS[j + i * Width] = Canny[j + i * Width];
		}
	}
	*/
#pragma endregion
/*
#pragma region Gradient Calculation

	double Gx = 0, Gy = 0;
	for (int i = 0; i < Height; i++) {
		for (int j = 0; j < Width; j++) {
			Gx = getConvolutionValue(imageGS.get(), j, i, Height, Width, SobelMultiplier, SobelKernelX);
			Gy = getConvolutionValue(imageGS.get(), j, i, Height, Width, SobelMultiplier, SobelKernelY);
			GD[j + i * Width] = Direction(Gx, Gy);
			Canny[j + i * Width] = (int)sqrt(Gx*Gx + Gy*Gy);
		}
	}
#pragma endregion

#pragma region  Non Maxima Supppresion 



	for (int i = 0; i < Height; i++)
	{
		for (int j = 0; j < Width; j++)
		{
			y = i + GD[j + i * Width].i;
			x = j + GD[j + i * Width].j;
			if (x >= 0 && x < Width && y >= 0 && y < Height) {
				if (Canny[x + y * Width] > Canny[j + i * Width]) {
					Mark[j + i * Width] = true;
					//					Sobel[j + i * Width] = 0;
				}
			}

			y = i - GD[j + i * Width].i;
			x = j - GD[j + i * Width].j;
			if (x >= 0 && x < Width && y >= 0 && y < Height) {
				if (Canny[x + y * Width] > Canny[j + i * Width]) {
					Mark[j + i * Width] = true;
					//					Sobel[j + i * Width] = 0;
				}
			}
		}
	}

	for (int i = 0; i < Height * Width; i++) {
		if (Mark[i]) Canny[i] = 0;
	}


#pragma endregion

#pragma region Otsu

	for (int i = 0; i < Height * Width; i++) {
		histogram[Canny[i]]++;
	}

	int total = Height * Width;
	int sum = 0;
	int sumB = 0;
	int wB = 0;
	int wF = 0;
	int mB;
	int mF;
	double max = 0.0;
	double between = 0.0;
	double threshold1 = 0.0;
	double threshold2 = 0.0;

	for (int i = 0; i < 256; i++) {
		sum += histogram[i] * i;
	}

	for (int i = 0; i < 256; i++) {
		wB += histogram[i];
		if (wB == 0)
			continue;
		wF = total - wB;
		if (wF == 0)
			break;
		sumB += i * histogram[i];
		mB = sumB / wB;
		mF = (sum - sumB) / wF;
		between = wB * wF * (mB - mF) * (mB - mF);
		if (between >= max) {
			threshold1 = i;
			if (between > max) {
				threshold2 = i;
			}
			max = between;
		}
	}

	OtsuThresholdH = (threshold1 + threshold2) / 2.0;
	OtsuThresholdL = OtsuThresholdH / 2;

#pragma endregion

#pragma region BLOB Analisys

	stack<Point> ce;
	bool cs = false;
	Mark.reset(new bool[Height * Width]());


	int k = -1;
	for (int i = 0; i < Height; i++)
	{
		for (int j = 0; j < Width; j++)
		{
			k++;
			if (Mark[k] == true) continue;
			Mark[k] = true;

			if (Canny[k] > OtsuThresholdH)
			{
				Canny[k] = 255;
				ce.push(Point(i, j));

				while (ce.size() > 0)
				{

					for (int ti = -1; ti <= 1; ti++)
					{
						for (int tj = -1; tj <= 1; tj++)
						{
							y = ce.top().i + ti;
							x = ce.top().j + tj;
							if ((x >= 0 && x < Width && y >= 0 && y < Height) && Mark[x + y * Width] == false) {
								if (Canny[x + y * Width] >= OtsuThresholdL) {
									Mark[x + y * Width] = true;
									ce.push(Point(y, x));
									Canny[x + y * Width] = 255;
								}
							}

						}
					}

					ce.pop();
				}

			}
		}
	}



#pragma endregion
*/
}


