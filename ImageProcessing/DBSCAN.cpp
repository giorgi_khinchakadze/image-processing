#include "stdafx.h"
#include "HelperClasses.h"
#include "Variable.h"
#include "Functions.h"

vector<Point> getNeighbours(int , int ,Lab, vector<Lab>&);


int r = 4;
int l = 0 - r;
int minPts = 40;




void PopulateDBSCAN() {

	vector<Lab> LabValues;
	unique_ptr<bool[]> Mark(new bool[Height * Width]());
	queue<Point> currentCluster;


	int nextCluster = 0;


	for (int i = 0, cp = 0; i < Height; i++)
	{
		for (int j = 0; j < Width; j++)
		{
			LabValues.push_back(Lab(imageR[cp], imageG[cp], imageB[cp]));
			cp++;
		}
	}



	for (int i = 0; i < Height; i++) 
	{
		for (int j = 0; j < Width; j++) 
		{
	
			if (Mark[j + i * Width] == false) 
			{
				Mark[j + i * Width] = true;
				vector<Point> tmpq = getNeighbours(j, i, LabValues[j + i * Width], LabValues);
				if (tmpq.size() > minPts)
				{
					for (Point p : tmpq){currentCluster.push(p);}

					nextCluster++;
					DBSCAN[j + i * Width] = nextCluster;

					while(currentCluster.size() > 0) 
					{
						Point tmp = currentCluster.front();
						if (Mark[tmp.j + tmp.i * Width] == false) 
						{
							Mark[tmp.j + tmp.i * Width] = true;
							
							tmpq = getNeighbours(tmp.j, tmp.i, LabValues[tmp.j + tmp.i * Width], LabValues);
							if (tmpq.size() > minPts) 
							{
								for (Point p : tmpq) {	currentCluster.push(p); }
							}
						}

						if (DBSCAN[tmp.j + tmp.i * Width] == 0) 
						{
							DBSCAN[tmp.j + tmp.i * Width] = nextCluster;
						}

						currentCluster.pop();

					}

				}


			}


		}
	}

}


vector<Point> getNeighbours(int x, int y,Lab p,vector<Lab>& values) {
	vector<Point> tmp(0);
	
	for (int i = l; i <= r; i++) {
		for (int j = l; j <= r; j++) {
			if ((x + j) >= 0 && (x + j) < Width && (y + i) >= 0 && (y + i) < Height) {
				if (p.isSimilar(values[(x + j) + (y + i) * Width]) == true) {
					tmp.push_back(Point(y + i, x + j));
				}
			}
		}
	}

	return tmp;
}




