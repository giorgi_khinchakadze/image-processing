#include "stdafx.h"
#include "HelperClasses.h"
#include "Variable.h"
#include "Functions.h"



// *************  Sobel ************* \\

double SobelMultiplier = 1;
vector< vector<double> > SobelKernelX{ { -1,0,1 },{ -2,0,2 } ,{ -1,0,1 } };
vector< vector<double> > SobelKernelY{ { -1,-2,-1 },{ 0,0,0 } ,{ 1,2,1 } };


// *************  Gaussian Filter ************* \\

double GaussianMultiplier = (1.00/159.00);
vector< vector<double > > GaussianKernel{ { 2,4,5,4,2 },{ 4,9,12,9,4 },{ 5,12,15,12,5 },{ 4,9,12,9,4 },{ 2,4,5,4,2 } };



void PopulateSobel() {

	unique_ptr<Direction[]> GD(new Direction[Height * Width]);
	unique_ptr<bool[]> Mark(new bool[Height * Width]());
	unique_ptr<int[]> histogram(new int[256]());
	int OtsuThresholdH = 0;
	int OtsuThresholdL = 0;
	int x, y;



#pragma region Gaussian Filter

	for (int i = 0; i < Height; i++) {
		for (int j = 0; j < Width; j++) {
			Canny[j + i * Width] = (int)getConvolutionValue(imageGS.get(), j, i,Height,Width, GaussianMultiplier, GaussianKernel);
		}
	}

	for (int i = 0; i < Height; i++) {
		for (int j = 0; j < Width; j++) {
			imageGS[j + i * Width] = Canny[j + i * Width];
		}
	}

#pragma endregion

#pragma region Gradient Calculation

	double Gx = 0, Gy = 0;
	for (int i = 0; i < Height; i++) {
		for (int j = 0; j < Width; j++) {
			Gx = getConvolutionValue(imageGS.get(), j, i, Height, Width, SobelMultiplier, SobelKernelX);
			Gy = getConvolutionValue(imageGS.get(), j, i, Height, Width, SobelMultiplier, SobelKernelY);
			GD[j + i * Width] = Direction(Gx, Gy);
			Canny[j + i * Width] = (int)sqrt(Gx*Gx + Gy*Gy);
		}
	}
#pragma endregion

#pragma region  Non Maxima Supppresion 


	
	for (int i = 0; i < Height; i++)
	{
		for (int j = 0; j < Width; j++)
		{
			y = i + GD[j + i * Width].i;
			x = j + GD[j + i * Width].j;
			if (x >= 0 && x < Width && y >= 0 && y < Height) {
				if (Canny[x + y * Width] > Canny[j + i * Width]) {
					Mark[j + i * Width] = true;
//					Sobel[j + i * Width] = 0;
				}
			}

			y = i - GD[j + i * Width].i;
			x = j - GD[j + i * Width].j;
			if (x >= 0 && x < Width && y >= 0 && y < Height) {
				if (Canny[x + y * Width] > Canny[j + i * Width]) {
					Mark[j + i * Width] = true;
//					Sobel[j + i * Width] = 0;
				}
			}
		}
	}

	for (int i = 0; i < Height * Width; i++) {
		if (Mark[i]) Canny[i] = 0;
	}
	

#pragma endregion

#pragma region Otsu

	for (int i = 0; i < Height * Width; i++) {
		histogram[Canny[i]]++;
	}

	int total = Height * Width;
	int sum = 0;
	int sumB = 0;
	int wB = 0;
	int wF = 0;
	int mB;
	int mF;
	double max = 0.0;
	double between = 0.0;
	double threshold1 = 0.0;
	double threshold2 = 0.0;

	for (int i = 0; i < 256; i++) {
		sum += histogram[i] * i;
	}

	for (int i = 0; i < 256; i++) {
		wB += histogram[i];
		if (wB == 0)
			continue;
		wF = total - wB;
		if (wF == 0)
			break;
		sumB += i * histogram[i];
		mB = sumB / wB;
		mF = (sum - sumB) / wF;
		between = wB * wF * (mB - mF) * (mB - mF);
		if (between >= max) {
			threshold1 = i;
			if (between > max) {
				threshold2 = i;
			}
			max = between;
		}
	}

	OtsuThresholdH = (threshold1 + threshold2) / 2.0;
	OtsuThresholdL = OtsuThresholdH / 2;

#pragma endregion

#pragma region BLOB Analisys

	stack<Point> ce;
	bool cs = false;
	Mark.reset(new bool[Height * Width]());


	int k = -1;
	for (int i = 0; i < Height; i++)
	{
		for (int j = 0; j < Width; j++)
		{
			k++;
			if (Mark[k] == true) continue;
			Mark[k] = true;

			if (Canny[k] > OtsuThresholdH) 
			{
				Canny[k] = 255;
				ce.push(Point(i,j));

				while (ce.size() > 0)
				{

					for (int ti = -1; ti <= 1; ti++) 
					{
						for (int tj = -1; tj <= 1; tj++) 
						{
							y = ce.top().i + ti;
							x = ce.top().j + tj;
							if ((x >= 0 && x < Width && y >= 0 && y < Height) && Mark[x + y * Width] == false ) {
								if (Canny[x + y * Width] >= OtsuThresholdL) {
									Mark[x + y * Width] = true;
									ce.push(Point(y, x));
									Canny[x + y * Width] = 255;
								}
							}

						}
					}

					ce.pop();
				}

			}
		}
	}



#pragma endregion

}



