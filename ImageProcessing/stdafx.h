// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// STL
#include <vector>
#include <iterator>
#include <fstream>
#include <string>
#include <cmath>
#include <memory>
#include <queue>
#include <stack>
#include <map>



using namespace std;

// TODO: reference additional headers your program requires here
