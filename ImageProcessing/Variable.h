#pragma once


extern unique_ptr<BYTE[]> imageGS;
extern unique_ptr<BYTE[]> imageR;
extern unique_ptr<BYTE[]> imageG;
extern unique_ptr<BYTE[]> imageB;
extern int Height;
extern int Width;


// *************  Returned Pointers ************* \\

extern unique_ptr<BYTE[]> Canny;

extern unique_ptr<BYTE[]> DBSCAN;

extern unique_ptr<BYTE[]> ChanVese;


extern ofstream fout;
