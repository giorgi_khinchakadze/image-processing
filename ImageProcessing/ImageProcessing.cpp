// ImageProcessing.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"

#include "DLL_Exports.h"
#include "Functions.h"

unique_ptr<BYTE[]> imageGS;
unique_ptr<BYTE[]> imageR;
unique_ptr<BYTE[]> imageG;
unique_ptr<BYTE[]> imageB;
int Height;
int Width;
ofstream fout("test.txt");



// *************  Returned Pointers ************* \\

unique_ptr<BYTE[]> Canny;

unique_ptr<BYTE[]> DBSCAN;

unique_ptr<BYTE[]> ChanVese;


API void SetImageDetails(void* data, int h, int w)
{
	BYTE* Data = (BYTE*)data;

	Height = h;
	Width = w;

	imageR.reset(new BYTE[h * w]());
	imageG.reset(new BYTE[h * w]());
	imageB.reset(new BYTE[h * w]());
	imageGS.reset(new BYTE[h * w]());

	int offset = h * w;

	for (int i = 0; i < Height * Width; i++)
	{
			imageR[i] = Data[i];
			imageG[i] = Data[i + offset];
			imageB[i] = Data[i + 2 * offset];
			imageGS[i] = Data[i + 3 * offset];
	}
}



API void* ProcessWithCanny()
{
	Canny.reset(new BYTE[Height*Width]());

	PopulateSobel();
	return Canny.get();

}

API void* ProcessWithDBSCAN() 
{

	DBSCAN.reset(new BYTE[Height*Width]());

	PopulateDBSCAN();

	return DBSCAN.get();

}

API void* ProcessWithChanVese() {

	ChanVese.reset(new BYTE[Height*Width]());

	PopulateChanVese();

	return ChanVese.get();

}











