﻿using System;
using System.Drawing;

namespace ImageProcessing
{
    public static class IPHelperFunctions
    {



        public static Byte[] toByteArray( Bitmap image )
        {
            Byte[] array = new Byte[image.Width * image.Height * 4 ];

            int cp = 0;
            int offset = image.Height * image.Width;

            for (int i = 0; i < image.Height; i++)
            {
                for ( int j = 0; j < image.Width; j ++)
                {
                    Color tmp = image.GetPixel(j, i);
                    array[cp] = tmp.R;
                    array[cp + offset] = tmp.G;
                    array[cp + 2 * offset] = tmp.B;
                    array[cp + 3 * offset] = (byte)(0.299 * (double)tmp.R + 0.587 * (double)tmp.G + 0.114 * (double)tmp.B);
                    cp++;
                }
            }

            return array;
        }



    }


}
