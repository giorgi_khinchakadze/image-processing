﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using ImageProcessing;
using System.Drawing;

namespace Image_Segmentation_Main_App
{
    static class Intermediary
    {

        private static Bitmap _Success = new Bitmap(Image.FromFile("D:/gio/My Progs/Image Segmentation Project/icons/success.png"));
        private static Bitmap _Error = new Bitmap(Image.FromFile("D:/gio/My Progs/Image Segmentation Project/icons/error.png"));

        private static bool _Parallel = false;

        private static bool _CannyDone = false;
        private static bool _GPUCannyDone = false;
        private static bool _DoCanny = false;

        private static bool _CVDone = false;
        private static bool _GPUCVDone = false;
        private static bool _DoChanVese = false;

        private static bool _DBSCANDone = false;
        private static bool _DoDBSCAN = false;


        #region Properties

        public static bool CannyDone
        {
            get
            {
                return _CannyDone;
            }

            set
            {
                _CannyDone = value;
            }
        }

        public static bool CVDone
        {
            get
            {
                return _CVDone;
            }

            set
            {
                _CVDone = value;
            }
        }

        public static bool DBSCANDone
        {
            get
            {
                return _DBSCANDone;
            }

            set
            {
                _DBSCANDone = value;
            }
        }

        public static bool DoCanny
        {
            get
            {
                return _DoCanny;
            }

            set
            {
                _DoCanny = value;
            }
        }



        public static bool DoChanVese
        {
            get
            {
                return _DoChanVese;
            }

            set
            {
                _DoChanVese = value;
            }
        }



        public static bool DoDBSCAN
        {
            get
            {
                return _DoDBSCAN;
            }

            set
            {
                _DoDBSCAN = value;
            }
        }

        public static Bitmap Success
        {
            get
            {
                return _Success;
            }

            set
            {
                _Success = value;
            }
        }

        public static Bitmap Error
        {
            get
            {
                return _Error;
            }

            set
            {
                _Error = value;
            }
        }

        public static bool Parallel
        {
            get
            {
                return _Parallel;
            }

            set
            {
                _Parallel = value;
            }
        }

        public static bool GPUCVDone
        {
            get
            {
                return _GPUCVDone;
            }

            set
            {
                _GPUCVDone = value;
            }
        }

        public static bool GPUCannyDone
        {
            get
            {
                return _GPUCannyDone;
            }

            set
            {
                _GPUCannyDone = value;
            }
        }
        #endregion

        public static void Process()
        {
            new Thread(new ThreadStart(() =>
            {
                if (_Parallel == false)
                {
                    if (_DoCanny == true && _CannyDone == false)
                    {
                        _CannyDone = true;
                        ImageProcessor.getCanny();
                    }
                    if (_DoDBSCAN == true && _DBSCANDone == false)
                    {
                        _DBSCANDone = true;
                        ImageProcessor.getDBSCAN();
                    }

                    if (DoChanVese == true && CVDone == false)
                    {
                        CVDone = true;
                        ImageProcessor.getChanVese();
                    }
                }
                else
                {
                    if (_DoCanny == true && _GPUCannyDone == false)
                    {
                        _GPUCannyDone = true;
                        ImageProcessor.getCannyGPU();
                    }
                    if (DoChanVese == true && _GPUCVDone == false)
                    {
                        _GPUCVDone = true;
                        ImageProcessor.getChanVeseGPU();
                    }



                }



            })).Start();

        }

        public static void GoFull()
        {
            new Thread(new ThreadStart(() => 
            { 
                MainView.view.Invoke(new Action(() => ImageView.Instance.Show()));
            }
            )).Start();
        }

        public static void ServerStatusUp()
        {
            MainView.ServerStatus.Invoke(new Action(() => { MainView.ServerStatus.Image = Success; MainView.ServerStatus.Update();  } ) );
        }
        public static void ServerStatusDown()
        {
            MainView.ServerStatus.Invoke(new Action(() => { MainView.ServerStatus.Image = Error; MainView.ServerStatus.Update(); }));

        }

    }
}
