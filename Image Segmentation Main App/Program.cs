﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using ImageProcessing;
using System.IO;

namespace Image_Segmentation_Main_App
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainView());
            }
            catch (Exception e)
            {
                File.AppendAllText("Main.log", "Exception While Communicating: " + e.Message + '\n');

            }




        }
    }
}
