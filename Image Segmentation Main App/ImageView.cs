﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;


namespace Image_Segmentation_Main_App
{
    public partial class ImageView : Form
    {
        private static PictureBox PB;
        private static ImageView instance;

        #region Properties

        public static ImageView Instance
        {
            get
            {
                return instance;
            }

            set
            {
                instance = value;
            }
        }

        #endregion

        public ImageView()
        {
            InitializeComponent();

            PB = Picture;

            this.KeyPress += (sender, args) => {
                if (args.KeyChar == 27)
                {
                    this.Hide();
                }
                if (args.KeyChar == '1')
                {
                    Execute(1);
                }
                if (args.KeyChar == '2')
                {
                    Execute(2);

                }
                if (args.KeyChar == '3')
                {
                    Execute(3);
                }
                if (args.KeyChar == '4')
                {
                    Execute(4);
                }


            };

        }


        public static void Execute(byte order)
        {
            MainView.view.Invoke(new Action(() =>
            {
                if (order.Equals(1))
                {
                    PB.Image = ImageProcessing.ImageProcessor.Image;
                    PB.Update();
                }
                if (order.Equals(2))
                {
                    PB.Image = ImageProcessing.ImageProcessor.CannyImage;
                    PB.Update();

                }
                if (order.Equals(3))
                {
                    PB.Image = ImageProcessing.ImageProcessor.DBSCANImage;
                    PB.Update();
                }
                if (order.Equals(4))
                {
                    PB.Image = ImageProcessing.ImageProcessor.ChanVeseImage;
                    PB.Update();
                }

            }));
        }
    }
}
