﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Imaging;
using ImageProcessing;
using System.Threading;

namespace Image_Segmentation_Main_App
{
    public partial class MainView : Form
    {
        public static PictureBox ServerStatus;
        public static MainView view;

        public MainView()
        {
            InitializeComponent();

            ServerStatus = status;
            ServerStatus.Update();

            ImageProcessor.CannyTimer.Elapsed += (sender, args) =>
            {
                ElapsedCanny.Invoke(new Action(() => ElapsedCanny.Text = String.Format("Elapsed Time: {0}s", ++ImageProcessor.CannyElapsedTime)));
            };
            ImageProcessor.DBSCANTimer.Elapsed += (sender, args) =>
            {
                ElapsedDBSCAN.Invoke(new Action(() => ElapsedDBSCAN.Text = String.Format("Elapsed Time: {0}s", ++ImageProcessor.DBSCANElapsedTime)));
            };



            ImageProcessor.ChanVeseTimer.Elapsed += (sender, args) =>
            {
                ElapsedDBSCAN.Invoke(new Action(() => ElapsedChanVese.Text = String.Format("Elapsed Time: {0}s", ++ImageProcessor.ChanVeselapsedTime)));
            };


            ServerStatus.Image = Intermediary.Error;

            ImageView.Instance = new ImageView();
            ImageView.Instance.Bounds = Screen.PrimaryScreen.Bounds;


            new Thread(new ThreadStart(Server.Server.StartUp)).Start();


        }


        private void LoadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Multiselect = false;
            dialog.Filter = "Image files (*.jpg, *.jpeg, *.bmp, *.png) | *.jpg; *.jpeg; *.bmp; *.png";
            dialog.ShowDialog();

            if (dialog.FileName.Length > 0)
            {
                PreImage.Load(dialog.FileName);
                ImageProcessor.Image = new Bitmap(Image.FromFile(dialog.FileName));
                new Thread(new ThreadStart(() => { ImageProcessor.StartUP(); })).Start();

                Intermediary.CannyDone = false;
                Intermediary.CVDone = false;
                Intermediary.DBSCANDone = false;
                Intermediary.GPUCannyDone = false;
                Intermediary.GPUCVDone = false;

            }

}

        private void ProcessButton_Click(object sender, EventArgs e)
        {
            Intermediary.Process();
        }
    

        private void Canny_Click(object sender, EventArgs e)
        {

            PostImage.Image = ImageProcessor.CannyImage;
            PostImage.Update();
        }

        private void MainView_Load(object sender, EventArgs e)
        {
            view = this;
        }
         
        private void GoFull_Click(object sender, EventArgs e)
        {
            Intermediary.GoFull();
        }

        private void DBSCAN_Click(object sender, EventArgs e)
        {
            PostImage.Image = ImageProcessor.DBSCANImage;
            PostImage.Update();

        }

        private void cannyBox_CheckedChanged(object sender, EventArgs e)
        {
            Intermediary.DoCanny = cannyBox.Checked;
        }

        private void DBSCANBox_CheckedChanged(object sender, EventArgs e)
        {
            Intermediary.DoDBSCAN = DBSCANBox.Checked;

        }

        private void ChanVeseBox_CheckedChanged(object sender, EventArgs e)
        {
            Intermediary.DoChanVese = ChanVeseBox.Checked;

        }

        private void ChanVese_Click(object sender, EventArgs e)
        {
            PostImage.Image = ImageProcessor.ChanVeseImage;
            PostImage.Update();

        }

        private void NPB_CheckedChanged(object sender, EventArgs e)
        {
            Intermediary.Parallel = PB.Checked;
        }

        private void PB_CheckedChanged(object sender, EventArgs e)
        {
            Intermediary.Parallel = PB.Checked;
        }
    }
}
