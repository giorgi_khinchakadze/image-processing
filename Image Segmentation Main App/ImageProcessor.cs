﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using Image_Segmentation_Main_App;
using System.Collections.Generic;
using System.Timers;


namespace ImageProcessing
{
    public class NativeMethods
    {
        //        [DllImport("D:/gio/My Progs/Image Segmentation Project/Debug/ImageProcessing.dll")]

        [DllImport("ImageProcessing.dll")]
        protected static extern void SetImageDetails(IntPtr data, int H, int W);
        [DllImport("ImageProcessing.dll")]
        protected static extern IntPtr ProcessWithCanny();
        [DllImport("ImageProcessing.dll")]
        protected static extern IntPtr ProcessWithDBSCAN();
        [DllImport("ImageProcessing.dll")]
        protected static extern IntPtr ProcessWithChanVese();


        [DllImport("ImageProcessingGPU.dll")]
        protected static extern void SetImageDetailsGPU(IntPtr data, int H, int W);
        [DllImport("ImageProcessingGPU.dll")]
        protected static extern IntPtr ProcessWithCannyGPU();
        [DllImport("ImageProcessingGPU.dll")]
        protected static extern IntPtr ProcessWithChanVeseGPU();




    }


    public class ImageProcessor : NativeMethods
    {
        private static Bitmap image;
        private static Byte[] imageData;


        private static Bitmap _CannyImage;
        private static System.Timers.Timer _CannyTimer = new System.Timers.Timer(1000);
        private static int _CannyElapsedTime;

        private static Bitmap _DBSCANImage;
        private static System.Timers.Timer _DBSCANTimer = new System.Timers.Timer(1000);
        private static int _DBSCANElapsedTime;

        private static Bitmap _ChanVeseImage;
        private static System.Timers.Timer _ChanVeseTimer = new System.Timers.Timer(1000);
        private static int _ChanVeselapsedTime;


        #region Properties
        public static Bitmap Image
        {
            get
            {
                return image;
            }

            set
            {
                image = value;
                imageData = IPHelperFunctions.toByteArray(image);
            }
        }

        public static Bitmap CannyImage
        {
            get
            {
                return _CannyImage;
            }

            private set
            {
                _CannyImage = value;
            }
        }

        public static System.Timers.Timer CannyTimer
        {
            get
            {
                return _CannyTimer;
            }

            set
            {
                _CannyTimer = value;
            }
        }

        public static int CannyElapsedTime
        {
            get
            {
                return _CannyElapsedTime;
            }

            set
            {
                _CannyElapsedTime = value;
            }
        }



        public static Bitmap DBSCANImage
        {
            get
            {
                return _DBSCANImage;
            }

            set
            {
                _DBSCANImage = value;
            }
        }

        public static System.Timers.Timer DBSCANTimer
        {
            get
            {
                return _DBSCANTimer;
            }

            set
            {
                _DBSCANTimer = value;
            }
        }

        public static int DBSCANElapsedTime
        {
            get
            {
                return _DBSCANElapsedTime;
            }

            set
            {
                _DBSCANElapsedTime = value;
            }
        }

        public static byte[] ImageData
        {
            get
            {
                return imageData;
            }

            set
            {
                imageData = value;
            }
        }

        public static Bitmap ChanVeseImage
        {
            get
            {
                return _ChanVeseImage;
            }

            set
            {
                _ChanVeseImage = value;
            }
        }

        public static System.Timers.Timer ChanVeseTimer
        {
            get
            {
                return _ChanVeseTimer;
            }

            set
            {
                _ChanVeseTimer = value;
            }
        }

        public static int ChanVeselapsedTime
        {
            get
            {
                return _ChanVeselapsedTime;
            }

            set
            {
                _ChanVeselapsedTime = value;
            }
        }

        #endregion



        public static void StartUP()
        {
            int size = Marshal.SizeOf(imageData[0]) * imageData.Length ;
            IntPtr ptr = Marshal.AllocHGlobal(size);
            Marshal.Copy(imageData, 0, ptr, imageData.Length);

            SetImageDetails(ptr, image.Height, image.Width);
            SetImageDetailsGPU(ptr, image.Height, image.Width);

            Marshal.FreeHGlobal(ptr);
        }




        public static void getCannyGPU()
        {

            _CannyTimer.Enabled = true;
            CannyImage = new Bitmap(image);
            IntPtr ptr = ProcessWithCannyGPU();

            int cp = 0;

            for (int i = 0; i < image.Height; i++)
            {
                for (int j = 0; j < image.Width; j++)
                {
                    Byte tmpb = Marshal.ReadByte(ptr, cp++);
                    Color tmpc = Color.FromArgb(tmpb, tmpb, tmpb);
                    _CannyImage.SetPixel(j, i, tmpc);
                }
            }

            _CannyTimer.Enabled = false;
            _CannyElapsedTime = 0;

        }


        public static void getCanny()
        {

            _CannyTimer.Enabled = true;
            CannyImage = new Bitmap(image);
            IntPtr ptr = ProcessWithCanny();

            int cp = 0;

            for (int i = 0; i < image.Height; i++)
            {
                for (int j = 0; j < image.Width; j++)
                {
                    Byte tmpb = Marshal.ReadByte(ptr, cp++);
                    Color tmpc = Color.FromArgb(tmpb, tmpb, tmpb);
                    _CannyImage.SetPixel(j, i, tmpc);
                }
            }

            _CannyTimer.Enabled = false;
            _CannyElapsedTime = 0;

        } 

        public static void getDBSCAN()
        {
            DBSCANTimer.Enabled = true;
            DBSCANImage = new Bitmap(image);
            IntPtr ptr = ProcessWithDBSCAN();

            int cp = 0;

            Dictionary<int, Color> clusters = new Dictionary<int, Color>();
            clusters.Add(0, Color.Black);

            for (int i = 0; i < image.Height; i++)
            {
                for (int j = 0; j < image.Width; j++)
                {

                    Byte tmpb = Marshal.ReadByte(ptr, cp++);
                    if (!clusters.ContainsKey(tmpb))
                    {
                        clusters.Add(tmpb, image.GetPixel(j,i));
                    }

                    _DBSCANImage.SetPixel(j, i, clusters[tmpb]);

                }
            }

            DBSCANTimer.Enabled = false;
            DBSCANElapsedTime = 0;


        }


        public static void getChanVese()
        {

            _ChanVeseTimer.Enabled = true;
            ChanVeseImage = new Bitmap(image);
            IntPtr ptr = ProcessWithChanVese();

            int cp = 0;

            for (int i = 0; i < image.Height; i++)
            {
                for (int j = 0; j < image.Width; j++)
                {
                    Byte tmpb = Marshal.ReadByte(ptr, cp++);
                    if (tmpb == 255)  _ChanVeseImage.SetPixel(j, i, Color.Red);
                }
            }

            _ChanVeseTimer.Enabled = false;
            _ChanVeselapsedTime = 0;

        }


        public static void getChanVeseGPU()
        {

            _ChanVeseTimer.Enabled = true;
            ChanVeseImage = new Bitmap(image);
            IntPtr ptr = ProcessWithChanVeseGPU();

            int cp = 0;

            for (int i = 0; i < image.Height; i++)
            {
                for (int j = 0; j < image.Width; j++)
                {
                    Byte tmpb = Marshal.ReadByte(ptr, cp++);
                    if (tmpb == 255) _ChanVeseImage.SetPixel(j, i, Color.Red);
                }
            }

            _ChanVeseTimer.Enabled = false;
            _ChanVeselapsedTime = 0;

        }

    }
}
