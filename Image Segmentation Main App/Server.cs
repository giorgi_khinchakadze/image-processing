﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;
using System.Net.Configuration;
using System.IO;
using Image_Segmentation_Main_App;


namespace Server

{
    static class Server
    {

        private static UdpClient server;
        private static IPEndPoint local;
        private static Thread serverThread;

        private static List<Thread> CurrentThreadList = new List<Thread>();


        public static void StartUp()
        {

            try
            {

                IPAddress address = null;

                foreach (NetworkInterface netint in NetworkInterface.GetAllNetworkInterfaces().Where(ipint => (ipint.NetworkInterfaceType == NetworkInterfaceType.Ethernet || ipint.NetworkInterfaceType == NetworkInterfaceType.Wireless80211) && ipint.OperationalStatus == OperationalStatus.Up))
                {
                    foreach (UnicastIPAddressInformation ip in netint.GetIPProperties().UnicastAddresses.Where(add => add.Address.AddressFamily == AddressFamily.InterNetwork))
                    {
                        address = ip.Address;
                    }
                }

                local = new IPEndPoint(address, 666);

                server = new UdpClient(local);
                server.EnableBroadcast = true;
                serverThread = new Thread(new ThreadStart(StartListening));
                serverThread.Start();
            }catch(Exception e)
            {
                File.AppendAllText("Server.log", "Exception While Communicating: " + e.Message + '\n');
            }
        }


        private static void StartListening()
        {

            while (true)
            {
                IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);
                Byte[] Data = server.Receive(ref remoteEndPoint);
                if (Data.Length != 2) continue;
                remoteEndPoint.Port = ((int)Data[0]) * 256 + (int)Data[1];
                new Thread(new ThreadStart(  ()=>Communicate(remoteEndPoint) )).Start();
            }

        }




        private static void Communicate(IPEndPoint remote)
        {
            Socket communicator = null;

            try
            {

                communicator = new Socket(SocketType.Stream, ProtocolType.Tcp);
                communicator.Connect(remote);
                Intermediary.ServerStatusUp();

                int size;
                Byte[] Data = new Byte[1024];

                while ((size = communicator.Receive(Data)) != 0)
                {
                    for (int i = 0; i < size; i++)
                    {
                        Execute(Data[i]);
                    }
                }



            }
            catch (Exception e)
            {
                File.AppendAllText("Server.log", "Exception While Communicating: " + e.Message + '\n');

            }
            finally
            {
                if (communicator != null)    communicator.Close();

                Intermediary.ServerStatusDown();
            }


        }


        private static void Execute(Byte order)
        {
            ImageView.Execute(order);
            if (order == 255) Intermediary.Process();
            if (order == 254) Intermediary.GoFull();
        }
    }
}
