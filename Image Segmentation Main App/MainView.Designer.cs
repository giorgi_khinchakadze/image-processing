﻿namespace Image_Segmentation_Main_App
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PreImage = new System.Windows.Forms.PictureBox();
            this.PostImage = new System.Windows.Forms.PictureBox();
            this.LoadButton = new System.Windows.Forms.Button();
            this.ProcessButton = new System.Windows.Forms.Button();
            this.Canny = new System.Windows.Forms.Button();
            this.ElapsedCanny = new System.Windows.Forms.Label();
            this.ElapsedDBSCAN = new System.Windows.Forms.Label();
            this.DBSCAN = new System.Windows.Forms.Button();
            this.ElapsedChanVese = new System.Windows.Forms.Label();
            this.ChanVese = new System.Windows.Forms.Button();
            this.GoFull = new System.Windows.Forms.Button();
            this.cannyBox = new System.Windows.Forms.CheckBox();
            this.DBSCANBox = new System.Windows.Forms.CheckBox();
            this.ChanVeseBox = new System.Windows.Forms.CheckBox();
            this.status = new System.Windows.Forms.PictureBox();
            this.NPB = new System.Windows.Forms.RadioButton();
            this.PB = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.PreImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostImage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.status)).BeginInit();
            this.SuspendLayout();
            // 
            // PreImage
            // 
            this.PreImage.Location = new System.Drawing.Point(12, 12);
            this.PreImage.Name = "PreImage";
            this.PreImage.Size = new System.Drawing.Size(255, 186);
            this.PreImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PreImage.TabIndex = 0;
            this.PreImage.TabStop = false;
            // 
            // PostImage
            // 
            this.PostImage.Location = new System.Drawing.Point(288, 12);
            this.PostImage.Name = "PostImage";
            this.PostImage.Size = new System.Drawing.Size(264, 186);
            this.PostImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PostImage.TabIndex = 1;
            this.PostImage.TabStop = false;
            // 
            // LoadButton
            // 
            this.LoadButton.Location = new System.Drawing.Point(99, 204);
            this.LoadButton.Name = "LoadButton";
            this.LoadButton.Size = new System.Drawing.Size(75, 52);
            this.LoadButton.TabIndex = 2;
            this.LoadButton.Text = "Load";
            this.LoadButton.UseVisualStyleBackColor = true;
            this.LoadButton.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // ProcessButton
            // 
            this.ProcessButton.Location = new System.Drawing.Point(380, 204);
            this.ProcessButton.Name = "ProcessButton";
            this.ProcessButton.Size = new System.Drawing.Size(75, 52);
            this.ProcessButton.TabIndex = 3;
            this.ProcessButton.Text = "Process";
            this.ProcessButton.UseVisualStyleBackColor = true;
            this.ProcessButton.Click += new System.EventHandler(this.ProcessButton_Click);
            // 
            // Canny
            // 
            this.Canny.Location = new System.Drawing.Point(569, 12);
            this.Canny.Name = "Canny";
            this.Canny.Size = new System.Drawing.Size(76, 51);
            this.Canny.TabIndex = 4;
            this.Canny.Text = "Canny";
            this.Canny.UseVisualStyleBackColor = true;
            this.Canny.Click += new System.EventHandler(this.Canny_Click);
            // 
            // ElapsedCanny
            // 
            this.ElapsedCanny.AutoSize = true;
            this.ElapsedCanny.Location = new System.Drawing.Point(651, 31);
            this.ElapsedCanny.Name = "ElapsedCanny";
            this.ElapsedCanny.Size = new System.Drawing.Size(88, 13);
            this.ElapsedCanny.TabIndex = 5;
            this.ElapsedCanny.Text = "Elapsed Time: 0s";
            // 
            // ElapsedDBSCAN
            // 
            this.ElapsedDBSCAN.AutoSize = true;
            this.ElapsedDBSCAN.Location = new System.Drawing.Point(651, 88);
            this.ElapsedDBSCAN.Name = "ElapsedDBSCAN";
            this.ElapsedDBSCAN.Size = new System.Drawing.Size(88, 13);
            this.ElapsedDBSCAN.TabIndex = 7;
            this.ElapsedDBSCAN.Text = "Elapsed Time: 0s";
            // 
            // DBSCAN
            // 
            this.DBSCAN.Location = new System.Drawing.Point(569, 69);
            this.DBSCAN.Name = "DBSCAN";
            this.DBSCAN.Size = new System.Drawing.Size(76, 51);
            this.DBSCAN.TabIndex = 6;
            this.DBSCAN.Text = "DBSCAN";
            this.DBSCAN.UseVisualStyleBackColor = true;
            this.DBSCAN.Click += new System.EventHandler(this.DBSCAN_Click);
            // 
            // ElapsedChanVese
            // 
            this.ElapsedChanVese.AutoSize = true;
            this.ElapsedChanVese.Location = new System.Drawing.Point(651, 145);
            this.ElapsedChanVese.Name = "ElapsedChanVese";
            this.ElapsedChanVese.Size = new System.Drawing.Size(88, 13);
            this.ElapsedChanVese.TabIndex = 11;
            this.ElapsedChanVese.Text = "Elapsed Time: 0s";
            // 
            // ChanVese
            // 
            this.ChanVese.Location = new System.Drawing.Point(569, 126);
            this.ChanVese.Name = "ChanVese";
            this.ChanVese.Size = new System.Drawing.Size(76, 51);
            this.ChanVese.TabIndex = 10;
            this.ChanVese.Text = "Chan-Vese";
            this.ChanVese.UseVisualStyleBackColor = true;
            this.ChanVese.Click += new System.EventHandler(this.ChanVese_Click);
            // 
            // GoFull
            // 
            this.GoFull.Location = new System.Drawing.Point(626, 208);
            this.GoFull.Name = "GoFull";
            this.GoFull.Size = new System.Drawing.Size(80, 51);
            this.GoFull.TabIndex = 12;
            this.GoFull.Text = "Go Full";
            this.GoFull.UseVisualStyleBackColor = true;
            this.GoFull.Click += new System.EventHandler(this.GoFull_Click);
            // 
            // cannyBox
            // 
            this.cannyBox.AutoSize = true;
            this.cannyBox.Location = new System.Drawing.Point(745, 27);
            this.cannyBox.Name = "cannyBox";
            this.cannyBox.Size = new System.Drawing.Size(15, 14);
            this.cannyBox.TabIndex = 13;
            this.cannyBox.UseVisualStyleBackColor = true;
            this.cannyBox.CheckedChanged += new System.EventHandler(this.cannyBox_CheckedChanged);
            // 
            // DBSCANBox
            // 
            this.DBSCANBox.AutoSize = true;
            this.DBSCANBox.Location = new System.Drawing.Point(745, 84);
            this.DBSCANBox.Name = "DBSCANBox";
            this.DBSCANBox.Size = new System.Drawing.Size(15, 14);
            this.DBSCANBox.TabIndex = 14;
            this.DBSCANBox.UseVisualStyleBackColor = true;
            this.DBSCANBox.CheckedChanged += new System.EventHandler(this.DBSCANBox_CheckedChanged);
            // 
            // ChanVeseBox
            // 
            this.ChanVeseBox.AutoSize = true;
            this.ChanVeseBox.Location = new System.Drawing.Point(745, 144);
            this.ChanVeseBox.Name = "ChanVeseBox";
            this.ChanVeseBox.Size = new System.Drawing.Size(15, 14);
            this.ChanVeseBox.TabIndex = 16;
            this.ChanVeseBox.UseVisualStyleBackColor = true;
            this.ChanVeseBox.CheckedChanged += new System.EventHandler(this.ChanVeseBox_CheckedChanged);
            // 
            // status
            // 
            this.status.Location = new System.Drawing.Point(758, 237);
            this.status.Name = "status";
            this.status.Size = new System.Drawing.Size(21, 22);
            this.status.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.status.TabIndex = 17;
            this.status.TabStop = false;
            // 
            // NPB
            // 
            this.NPB.AutoSize = true;
            this.NPB.Checked = true;
            this.NPB.Location = new System.Drawing.Point(569, 183);
            this.NPB.Name = "NPB";
            this.NPB.Size = new System.Drawing.Size(82, 17);
            this.NPB.TabIndex = 18;
            this.NPB.TabStop = true;
            this.NPB.Text = "Non Parallel";
            this.NPB.UseVisualStyleBackColor = true;
            this.NPB.CheckedChanged += new System.EventHandler(this.NPB_CheckedChanged);
            // 
            // PB
            // 
            this.PB.AutoSize = true;
            this.PB.Location = new System.Drawing.Point(675, 185);
            this.PB.Name = "PB";
            this.PB.Size = new System.Drawing.Size(59, 17);
            this.PB.TabIndex = 19;
            this.PB.Text = "Parallel";
            this.PB.UseVisualStyleBackColor = true;
            this.PB.CheckedChanged += new System.EventHandler(this.PB_CheckedChanged);
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(791, 271);
            this.Controls.Add(this.PB);
            this.Controls.Add(this.NPB);
            this.Controls.Add(this.status);
            this.Controls.Add(this.ChanVeseBox);
            this.Controls.Add(this.DBSCANBox);
            this.Controls.Add(this.cannyBox);
            this.Controls.Add(this.GoFull);
            this.Controls.Add(this.ElapsedChanVese);
            this.Controls.Add(this.ChanVese);
            this.Controls.Add(this.ElapsedDBSCAN);
            this.Controls.Add(this.DBSCAN);
            this.Controls.Add(this.ElapsedCanny);
            this.Controls.Add(this.Canny);
            this.Controls.Add(this.ProcessButton);
            this.Controls.Add(this.LoadButton);
            this.Controls.Add(this.PostImage);
            this.Controls.Add(this.PreImage);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MainView";
            this.Text = "MainView";
            this.Load += new System.EventHandler(this.MainView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.PreImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PostImage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.status)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PreImage;
        private System.Windows.Forms.PictureBox PostImage;
        private System.Windows.Forms.Button LoadButton;
        private System.Windows.Forms.Button ProcessButton;
        private System.Windows.Forms.Button Canny;
        private System.Windows.Forms.Label ElapsedCanny;
        private System.Windows.Forms.Label ElapsedDBSCAN;
        private System.Windows.Forms.Button DBSCAN;
        private System.Windows.Forms.Label ElapsedChanVese;
        private System.Windows.Forms.Button ChanVese;
        private System.Windows.Forms.Button GoFull;
        private System.Windows.Forms.CheckBox cannyBox;
        private System.Windows.Forms.CheckBox DBSCANBox;
        private System.Windows.Forms.CheckBox ChanVeseBox;
        private System.Windows.Forms.PictureBox status;
        private System.Windows.Forms.RadioButton NPB;
        private System.Windows.Forms.RadioButton PB;
    }
}